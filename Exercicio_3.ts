/*3-) Faça um programa que receba três números obrigatoriamente em ordem crescente e um quarto número que não siga esta regra. Mostre, em seguida, os quatro números em ordem decrescente.*/

namespace exercicio_3 {
  let numero1, numero2, numero3, numeroZ: number;
  numero1 = 2;
  numero2 = 4;
  numero3 = 6;
  numeroZ = 3;

  if (numeroZ > numero3) {
    console.log(`Sequência decrescente: ${numeroZ}, ${numero3}, ${numero2}, ${numero1}`);
  } else if (numeroZ > numero2) {
    console.log(`Sequência decrescente: ${numero3}, ${numeroZ}, ${numero2}, ${numero1}`);
  } else if (numeroZ > numero1) {
    console.log(`Sequência decrescente: ${numero3}, ${numero2}, ${numeroZ}, ${numero1}`);
  }
  else{
    console.log(`Sequência decrescente: ${numero3}, ${numero2}, ${numero1}, ${numeroZ}`);
  }
}

namespace exemplos_condicoes {
    let idade: number = 10;
    //Normal
    if(idade >= 18) 
    {//inicio bloco
        console.log("Pode dirigir");
    }//fim bloco 
    else
    {
        console.log("Não pode dirigir");  
    }

    //Ternário
    idade >= 18 ? console.log("pode dirigir") : console.log("Não pode dirigir");

}